provider "google" {
  project = "terraform-379914"
  region  = "us-central1"
  zone    = "us-central1-c"
}

terraform {
  backend "gcs" {
    bucket = "lewis-terraform-bucket"
    prefix = "terraform/state"
  }
}

